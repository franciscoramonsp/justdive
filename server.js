'use strict'

const crypter = require('./core/Crypter');
const express = require('express');
const bodyParser = require('body-parser');

const app = express();

const port = process.env.PORT || 6660;
const Crypter = new crypter()

app.use(bodyParser.json({limit: '20mb'}));
var jsonParser = bodyParser.json()

app.post('/encrypt', jsonParser, (req, res) => {
    const encrypted_key = Crypter.encrypt(JSON.stringify(req.body));   
    res.setHeader('Content-Type', 'application/json');
    const response = {
        param: encrypted_key
    }
    res.send(response);
});

app.listen(port,'0.0.0.0');
console.log('Server started! At http://localhost:' + port);
