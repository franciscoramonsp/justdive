const crypto = require('crypto');

class Crypter{

    constructor(){
        this.ENC_KEY = "AAAD8B4C00E13F53B8E716BCB5F4D1B9";
        this.IV = "FA0E3C9B4FA2C7AA"
    }

    encrypt(val){
        const cipher = crypto.createCipheriv('aes-256-cbc', this.ENC_KEY, this.IV);
        let encrypted = cipher.update(val, 'utf8', 'hex');
        encrypted += cipher.final('hex');
        return encrypted;
    }

    decrypt(encrypted){
        const decipher = crypto.createDecipheriv('aes-256-cbc', this.ENC_KEY, this.IV);
        let decrypted = decipher.update(encrypted, 'hex', 'utf8');
        return (decrypted + decipher.final('utf8'));
    }
}

module.exports = Crypter;
